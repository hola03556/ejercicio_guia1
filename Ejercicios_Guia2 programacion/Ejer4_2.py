#Le píde una frase por la terminal
Texto = str(input("Ingrese su oracion: "))
#Se cambia a misnuculas todos los caracteres de la variable str
Texto = Texto.lower()
# Le píde una palabra o valor por la terminal para buscarla en la oracion
Palabra = str(input("Ingrese la palabra que quiere buscar: "))
Palabra = Palabra.lower()
#Condicion si la palabra existe
if Palabra in Texto:
    print("La palabra existe en el texto")
    #Muestra la posicion de la palabras
    print("Desde la posicion: ",Texto.find(Palabra))
else:
    print("La palabra no existe en el texto")
