#Se le agrega el mensaje secreto  a una variable
texto = "XeXgXaXsXsXeXmX XtXeXrXcXeXsX XaX XsXiX XsXiXhXt"
print(texto)
#Se le asigna la longitud del str que guarda el mensaje secreto
B = len(texto)
#Se crea un str
Mensaje = ""
#Ciclo recorre el mensaje secreto de atras a adelante 
while B > 0:
    #Condicion si el valor del indice es distinto de X, se cumple y entra
    if texto[B - 1] != "X":
        #Se le agrega a Mensaje tipo de dato str
        Mensaje += texto[B-1]
    #Disminuye en uno cada vez que da una vuelta
    B = B - 1
#Imprime el Mensaje resuelto
print(Mensaje)