#Se le pide la palabra a adivinar en la terminal
Palabra_Adivinar = str(input("Ingrese su palabra: "))
#Se le cambiar las letras por todas minusculas
Palabra_Adivinar = Palabra_Adivinar.lower()
#se define una variable que seria las vidas del usuario
vidas = 5
#Se inicializa
Final = 0
#Se crea una lista que compruebe si se completo la palabra secreta
cadena_espacios = []
#Se crea una lista que representa el progreso del usuario 
progreso = []
#ciclo que crea la lista que se imprimira en la terminal
for i in range(len(Palabra_Adivinar)):
    progreso.append("_ ")
#Se crea una lista que almacenara las letras usadas
letras_ocupadas = []
##ciclo que crea la lista que afirma si se completo la palabra
for char in Palabra_Adivinar:
    cadena_espacios.append(char + " ")

#Ciclo que continua si se pierden las vidas o si gana el usuario
while (vidas > 0) and (Final == 0 ):
    print("*************************")
    #Imprime las vidas del usuario
    print("\u2764\ufe0f " * vidas)
    #convierte la lista progreso en un string y lo imprime en pantalla
    print(" ".join(progreso))

    print("Letras usadas: ",letras_ocupadas)
    
    #Le pide un valor por la terminal
    Letra = input("Elige una letra: ")

    if Letra in letras_ocupadas:
        print("Esta letra ya lo uso...")
    else:
        #Agrela la letra en la lista correspondiente
        letras_ocupadas.append(Letra)
        #Si el progreso y la cadena son iguales termina el ciclo y sale
    if cadena_espacios == progreso:
        Final = 1
    #Se inicializa
    hay_error = True 
    #Recorre la palabra secreta
    for i in range(len(Palabra_Adivinar)):
        if Letra == Palabra_Adivinar[i]:
            progreso[i] = Letra + " "
            hay_error = False
        
    #Si error es verdad se le resta una vida
    if hay_error:
        vidas -=1

if Final == 1:
    print("\n\n\n Ganaste")
else:
    print("\n\n\n Lo siento, Perdiste")


     