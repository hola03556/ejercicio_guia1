from os import system
from time import sleep
Speed = 3
#-----------------Funcion Farmaceutica Dr. Simi---------------
def abrir_archivo(nombre=None):
    Archivo = open("Farmaceutica_Dr_Simi.txt","w")
    return Archivo
#-----------------Funcion Laboratorios-----------------
def laboratorio_Opko(nombre=None):
    Lab = open("Laboratorio_Opko.txt","r")
    return Lab
def laboratorio_Chile(nombre=None):
    Lab = open("Laboratorio_Chile.txt","r")
    return Lab
def laboratorio_Pasteur(nombre=None):
    Lab = open("Laboratorio_Pasteur.txt","r")
    return Lab
#------------------- Funcion del Contenido Farmaceutica---------------
def contenido_Farmacia(archivo=None,lab1=None,lab2=None,lab3=None):
    print("\t---------------------------------------")
    print("\t-         Farmacia Dr. Simi           -")
    print("\t---------------------------------------")
    i = 0
    texto = ""
    if i == 0:
        print("====================================================================================")
        texto = texto + f"=*******Laboratorio_Opko******=\n"
        for linea1 in lab1:
            Medicamento = linea1.strip().split(":")
            Codigo = Medicamento[0]
            nombre = Medicamento[1]
            Componente = Medicamento[2]
            Precio = Medicamento[3]
            texto = texto + f"Codigo del Producto: {Codigo}\n"
            texto = texto + f"Nombre del Fármaco: {nombre}\n"
            texto = texto + f"Componente principal: {Componente}\n"
            texto = texto + f"Precio de venta: {Precio}\n"
            texto = texto + f"-------------------------------------------------------------\n"
        i = 1
        print("====================================================================================")
    if i == 1:
        texto = texto + f"=*******Laboratorio_Chile******=\n"
        for linea2 in lab2:
            Medicamento = linea2.strip().split(":")
            Codigo = Medicamento[0]
            nombre = Medicamento[1]
            Componente = Medicamento[2]
            Precio = Medicamento[3]
            texto = texto + f"Codigo del Producto: {Codigo}\n"
            texto = texto + f"Nombre del Fármaco: {nombre}\n"
            texto = texto + f"Componente principal: {Componente}\n"
            texto = texto + f"Precio de venta: {Precio}\n"
            texto = texto + f"-------------------------------------------------------------\n"
        i = 2
        print("====================================================================================")
    if i == 2:
        texto = texto + f"=*******Laboratorio_Pasteur******=\n"
        for linea3 in lab3:
            Medicamento = linea3.strip().split(":")
            Codigo = Medicamento[0]
            nombre = Medicamento[1]
            Componente = Medicamento[2]
            Precio = Medicamento[3]
            texto = texto + f"Codigo del Producto: {Codigo}\n"
            texto = texto + f"Nombre del Fármaco: {nombre}\n"
            texto = texto + f"Componente principal: {Componente}\n"
            texto = texto + f"Precio de venta: {Precio}\n"
            texto = texto + f"-------------------------------------------------------------\n"
    archivo.write(texto)
    print(texto)
    print("\n\n\n\n")
#-------------------Agregar medicamento a uno de los laboratorios-----------------------
def agregar_medicamento(lab1=None,lab2=None,lab3=None):
     
    print("1. Laboratorio Opko")
    print("2. Laboratorio Chile")
    print("3. Laboratorio Pasteur")
    #Se le pide al usuario que indique el laboratorio que quiere agregar el nuevo medicamento
    laboratorio = int(input("--->"))
    system("cls")
    texto = ""
    if laboratorio == 1:
        #Se abre el archivo correspondien al laboratorio elegido
        Lab = open("Laboratorio_Opko.txt","r")
        #Se crea una cadena que imprime en la terminal el contenido del laboratorio en el archivo de Farmacio Dr. Simi
        texto = texto + f"*******Laboratorio_Opko******\n"
        for linea12 in Lab:
            #Se le da un valor a una variable por cada linea del archivo del laboratorio
            Medicamento = linea12.strip().split(":")
            Codigo = Medicamento[0]
            nombre = Medicamento[1]
            Componente = Medicamento[2]
            Precio = Medicamento[3]
            #Se une cada oracion en la varible como cadena
            texto = texto + f"Codigo del Producto: {Codigo}\n"
            texto = texto + f"Nombre del Fármaco: {nombre}\n"
            texto = texto + f"Componente principal: {Componente}\n"
            texto = texto + f"Precio de venta: {Precio}\n"
            texto = texto + f"-------------------------------------------------------------\n"
        #Se guarda el contenido del archivo
        Lab.close()
        #Se abre el archivo del laboratorio
        lab = open("Laboratorio_Opko.txt","a")
        #Se le pide al usuario cada valor que se le pide sobre el medicamento que quiere agregar
        Codigo = int(input("* Agregue el codigo del Medicamento:"))
        nombre = input("* Ingrese el nombre de su medicamento:")
        nombre = nombre.upper()
        Componente = input("* Ingrese el componente principal:")
        Componente = Componente.upper()
        precio = int(input("* Ingrese el precio del medicamento:"))
        texto1 = f"F-{Codigo}:{nombre}:{Componente}:${precio}\n"
        #Se le agrega al archivo y posteriormente se cierra para guardar
        lab.write(texto1)
        lab.close()
        print("\n\n\n\n")

    elif laboratorio == 2:
        #Se le da un valor a una variable por cada linea del archivo del laboratorio
        Lab = open("Laboratorio_Chile.txt","r")
        texto = texto + f"*******Laboratorio_Chile******\n"
        for linea9 in Lab:
            Medicamento = linea9.strip().split(":")
            Codigo = Medicamento[0]
            nombre = Medicamento[1]
            Componente = Medicamento[2]
            Precio = Medicamento[3]
            #Se une cada oracion en la varible como cadena
            texto = texto + f"Codigo del Producto: {Codigo}\n"
            texto = texto + f"Nombre del Fármaco: {nombre}\n"
            texto = texto + f"Componente principal: {Componente}\n"
            texto = texto + f"Precio de venta: {Precio}\n"
            texto = texto + f"-------------------------------------------------------------\n"
            #Se guarda el contenido del archivo
            Lab.close()
        lab = open("Laboratorio_Chile.txt","a")
        #Se le pide al usuario cada valor que se le pide sobre el medicamento que quiere agregar
        Codigo = int(input("* Agregue el codigo del Medicamento:"))
        nombre = input("* Ingrese el nombre de su medicamento:")
        nombre = nombre.upper()
        Componente = input("* Ingrese el componente principal:")
        Componente = Componente.upper()
        precio = int(input("* Ingrese el precio del medicamento:"))
        texto1 = f"F-{Codigo}:{nombre}:{Componente}:${precio}\n"
        lab.write(texto1)
        lab.close()
        print("\n\n\n\n")

    elif laboratorio == 3:
        #Se le da un valor a una variable por cada linea del archivo del laboratorio
        Lab = open("Laboratorio_Pasteur.txt","r")
        texto = texto + f"*******Laboratorio_Pasteur******\n"
        for linea10 in Lab:
            Medicamento = linea10.strip().split(":")
            Codigo = Medicamento[0]
            nombre = Medicamento[1]
            Componente = Medicamento[2]
            Precio = Medicamento[3]
            #Se une cada oracion en la varible como cadena
            texto = texto + f"Codigo del Producto: {Codigo}\n"
            texto = texto + f"Nombre del Fármaco: {nombre}\n"
            texto = texto + f"Componente principal: {Componente}\n"
            texto = texto + f"Precio de venta: {Precio}\n"
            texto = texto + f"-------------------------------------------------------------\n"
            #Se guarda el contenido del archivo
            Lab.close()
        #Se le pide al usuario cada valor que se le pide sobre el medicamento que quiere agregar
        lab = open("Laboratorio_Pasteur.txt","a")
        Codigo = int(input("* Agregue el codigo del Medicamento:"))
        nombre = input("* Ingrese el nombre de su medicamento:")
        nombre = nombre.upper()
        Componente = input("* Ingrese el componente principal:")
        Componente = Componente.upper()
        precio = int(input("* Ingrese el precio del medicamento:"))
        texto1 = f"F-{Codigo}:{nombre}:{Componente}:${precio}\n"
        lab.write(texto1)
        lab.close()
        print("\n\n\n\n")

    
#---------------------Consultar el precio de un medicamento independiente de la marca ----------              
def Consultar_precio(lab1=None,lab2=None,lab3=None):
    consulta = str(input("Ingrese el nombre del producto: "))
    consulta = consulta.upper()
    texto5 = ""
    #Se abre todos los archivos relacionados con los archivos de laboratorio
    lab1 = open("Laboratorio_Opko.txt")
    lab2 = open("Laboratorio_Chile.txt")
    lab3 = open("Laboratorio_Pasteur.txt")
    #Se recorre cada linea del archivo
    for palabra in lab1:
        #Se transforma la linea en una cadena a una lista
        Medicamento = palabra.strip().split(":")
        nombre = Medicamento[2]
        Precio = Medicamento[3]
        #Si la condicion es verdadera, se procedea a hacer lo que hay dentro
        if consulta == nombre:
            texto5 = texto5 + f"*******Laboratorio_Opko******\n"
            texto5 = texto5 + f"Nombre del Fármaco: {Medicamento[1]}\n"
            texto5 = texto5 + f"Precio de venta: {Precio}\n"
            #Se guarda el contenido
            lab1.close()
            print("\n\n\n\n")
            break
    #Se recorre cada linea del archivo
    for palabra2 in lab2:
        #Se transforma la linea en una cadena a una lista
        Medicamento = palabra2.strip().split(":")
        nombre = Medicamento[2]
        Precio = Medicamento[3]
        #Si la condicion es verdadera, se procedea a hacer lo que hay dentro
        if consulta == nombre:
            texto5 = texto5 + f"*******Laboratorio_Chile******\n"
            texto5 = texto5 + f"Nombre del Fármaco: {Medicamento[1]}\n"
            texto5 = texto5 + f"Precio de venta: {Precio}\n"
            #Se guarda el contenido
            lab2.close()
            print("\n\n\n\n")
            break
    #Se recorre cada linea del archivo
    for palabra3 in lab3:
        #Se transforma la linea en una cadena a una lista
        Medicamento = palabra3.strip().split(":")
        nombre = Medicamento[2]
        Precio = Medicamento[3]
        #Si la condicion es verdadera, se procedea a hacer lo que hay dentro
        if consulta in nombre: 
            texto5 = texto5 + f"*******Laboratorio_Pasteur******\n"
            texto5 = texto5 + f"Nombre del Fármaco: {Medicamento[1]}\n"
            texto5 = texto5 + f"Precio de venta: {Precio}\n"
            #Se guarda el contenido del archivo
            lab3.close()
            print("\n\n\n\n")
            break
    #Se imprime el contenido de la variable
    print(texto5)
    print("\n\n\n\n")

#---------------------Editar el contenido de un medicamento dependiendo del Laboratorio ---------- 
def Editar_Medicamento(lab1=None,lab2=None,lab3=None):
    print("1. Laboratorio Opko")
    print("2. Laboratorio Chile")
    print("3. Laboratorio Pasteur")
    #Se le pide al usuario que ingrese el laboratorio que quiere y el medicamento de ese laboratorio
    laboratorio = int(input("--->"))
    Medicamento = str(input("* Ingrese su medicamento a editar: "))
    Medicamento = Medicamento.upper()
    #Si es el laboratorio , se cumple
    if laboratorio == 1:
        lab1 = open("Laboratorio_Opko.txt")
        Nuevo_texto = ""
        #Se recorre cada linea del archivo
        for estrofa in lab1:
            Medicament = estrofa.strip().split(":")
            #Cuando es el medicamento, se cumple
            #en vez que se agregue al archivo , se modifica por el usuario
            if (Medicament[2] == Medicamento) or (Medicament[1] == Medicamento):
                Codigo = int(input("* Agregue el codigo del Medicamento:"))
                nombre = input("* Ingrese el nombre de su medicamento:")
                nombre = nombre.upper()
                Componente = input("* Ingrese el componente principal:")
                Componente = Componente.upper()
                precio = int(input("* Ingrese el precio del medicamento:"))
                Nuevo = f"F-{Codigo}:{nombre}:{Componente}:${precio}\n"
                Nuevo_texto = Nuevo_texto + Nuevo
                continue
            #Todo se almacena en una variable que almanece el nuevo contenido del archivo
            Nuevo_texto = Nuevo_texto + estrofa
            #Se guarda el archivo que estaba en modo lectura
        lab1.close()
        #Se abre el archivo de modo write, borrara todo el contenido del archivo anterior
        lab1 = open("Laboratorio_Opko.txt","w")
        #Pero con la variable que tiene el nuevo contenido del archivo, se escribe en el archivo que no tiene nada
        lab1.write(Nuevo_texto)
        # Posteriormente se cierra, y asi guardando el nuevo contenido
        lab1.close()
    #Si la condicion es verdadera, se procedea a hacer lo que hay dentro
    elif laboratorio == 2:
        lab2 = open("Laboratorio_Chile.txt")
        Nuevo_texto = ""
        for estrofa2 in lab2:
            Medicament = estrofa2.strip().split(":")
            #Cuando es el medicamento, se cumple
            #en vez que se agregue al archivo , se modifica por el usuario
            if (Medicament[2] == Medicamento) or (Medicament[1] == Medicamento):
                Codigo = int(input("* Agregue el codigo del Medicamento:"))
                nombre = input("* Ingrese el nombre de su medicamento:")
                nombre = nombre.upper()
                Componente = input("* Ingrese el componente principal:")
                Componente = Componente.upper()
                precio = int(input("* Ingrese el precio del medicamento:"))
                Nuevo = f"F-{Codigo}:{nombre}:{Componente}:${precio}\n"
                Nuevo_texto = Nuevo_texto + Nuevo
                continue
            #Todo se almacena en una variable que almanece el nuevo contenido del archivo
            Nuevo_texto = Nuevo_texto + estrofa2
            #Se guarda el archivo que estaba en modo lectura
        lab2.close()
        #Se abre el archivo de modo write, borrara todo el contenido del archivo anterior
        lab2 = open("Laboratorio_Chile.txt","w")
        #Pero con la variable que tiene el nuevo contenido del archivo, se escribe en el archivo que no tiene nada
        lab2.write(Nuevo_texto)
        lab2.close()
    #Si la condicion es verdadera, se procedea a hacer lo que hay dentro
    elif laboratorio == 3:
        lab2 = open("Laboratorio_Chile.txt")
        Nuevo_texto = ""
        for estrofa3 in lab3:
            Medicament = estrofa3.strip().split(":")
            if (Medicament[2] == Medicamento) or (Medicament[1] == Medicamento):
                Codigo = int(input("* Agregue el codigo del Medicamento:"))
                nombre = input("* Ingrese el nombre de su medicamento:")
                nombre = nombre.upper()
                Componente = input("* Ingrese el componente principal:")
                Componente = Componente.upper()
                precio = int(input("* Ingrese el precio del medicamento:"))
                Nuevo = f"F-{Codigo}:{nombre}:{Componente}:${precio}\n"
                Nuevo_texto = Nuevo_texto + Nuevo
                continue
            Nuevo_texto = Nuevo_texto + estrofa3
            #Todo se almacena en una variable que almanece el nuevo contenido del archivo
            #Se guarda el archivo que estaba en modo lectura
        lab3.close()
        #Se abre el archivo de modo write, borrara todo el contenido del archivo anterior
        lab3 = open("Laboratorio_Pasteur.txt","w")
        #Pero con la variable que tiene el nuevo contenido del archivo, se escribe en el archivo que no tiene nada
        lab3.write(Nuevo_texto)
        # Posteriormente se cierra, y asi guardando el nuevo contenido
        lab3.close()    

    
#---------------------Eliminar el medicamento elegido respecto al Laboratorio escogido ---------- 
def Eliminar_medicamento(lab1=None,lab2=None,lab3=None):
    print("1. Laboratorio Opko")
    print("2. Laboratorio Chile")
    print("3. Laboratorio Pasteur")
     #Pero con la variable que tiene el nuevo contenido del archivo, se escribe en el archivo que no tiene nada
    laboratorio = int(input("--->"))
    Medicamento = str(input("* Ingrese su medicamento a eliminar: "))
    Medicamento = Medicamento.upper()
    if laboratorio == 1:
        #Se abre el archivo correspondiente
        lab1 = open("Laboratorio_Opko.txt")
        Nuevo_texto = ""
        for estrofa in lab1:
            Medicament = estrofa.strip().split(":")
            #Se abre el archivo correspondiente
            if (Medicament[2] != Medicamento) or (Medicament[1] != Medicamento):
             Nuevo_texto = Nuevo_texto + estrofa
        lab1.close()
        lab1 = open("Laboratorio_Opko.txt","w")
        lab1.write(Nuevo_texto)
        lab1.close()     
    elif laboratorio == 2:
        #Se abre el archivo correspondiente
        lab2 = open("Laboratorio_Chile.txt")
        Nuevo_texto = ""
        for estrofa2 in lab2:
            #Se guarda todos los elementos , excepto elelementoq que nose quiere guardar
            Medicament = estrofa2.strip().split(":")
            if (Medicament[2] != Medicamento) or (Medicament[1] != Medicamento):
             Nuevo_texto = Nuevo_texto + estrofa2
        lab2.close()
        lab2 = open("Laboratorio_Chile.txt","w")
        lab2.write(Nuevo_texto)
        lab2.close()
        #Se abre el archivo correspondiente
    elif laboratorio == 3:
        lab3 = open("Laboratorio_Pasteur.txt")
        Nuevo_texto = ""
        for estrofa3 in lab3:
            Medicament = estrofa3.strip().split(":")
            #Se abre el archivo correspondiente
            if (Medicament[2] != Medicamento) or (Medicament[1] != Medicamento):
             Nuevo_texto = Nuevo_texto + estrofa3
        lab3.close()
        lab3 = open("Laboratorio_Pasteur.txt","w")
        lab3.write(Nuevo_texto)
        lab3.close()    
#--------------------- Decir el mejor medicamento especifico respecto al precio ---------- 
def Mejor_eleccion(lab1=None,lab2=None,lab3=None):
    Mejor_elecci = str(input("Ingrese el nombre del producto: "))
    Mejor_elecci = Mejor_elecci.upper()
    texto5 = ""
    texto6 = ""
    texto7 = ""
    lab1 = open("Laboratorio_Opko.txt")
    lab2 = open("Laboratorio_Chile.txt")
    lab3 = open("Laboratorio_Pasteur.txt")
    for palabra in lab1:
        Medicamento = palabra.strip().split(":")
        nombre = Medicamento[2]
        Precio1 = Medicamento[3]
        if Mejor_elecci == nombre:
            texto5 = texto5 + f"*******Laboratorio_Opko******\n"
            texto5 = texto5 + f"Nombre del Fármaco: {Medicamento[1]}\n"
            texto5 = texto5 + f"Precio de venta: {Precio1}\n"
            break
    for palabra2 in lab2:
        Medicamento = palabra2.strip().split(":")
        nombre = Medicamento[2]
        Precio2 = Medicamento[3]
        if Mejor_elecci == nombre:
            texto6 = texto6 + f"*******Laboratorio_Chile******\n"
            texto6 = texto6 + f"Nombre del Fármaco: {Medicamento[1]}\n"
            texto6 = texto6 + f"Precio de venta: {Precio2}\n"
            break
    for palabra3 in lab3:
        Medicamento = palabra3.strip().split(":")
        nombre = Medicamento[2]
        Precio3 = Medicamento[3]
        if Mejor_elecci == nombre: 
            texto7 = texto7 + f"*******Laboratorio_Pasteur******\n"
            texto7 = texto7 + f"Nombre del Fármaco: {Medicamento[1]}\n"
            texto7 = texto7 + f"Precio de venta: {Precio3}\n"
            break
    temp1 = Precio1
    temp1 = temp1 + Precio2
    temp1 = temp1 + Precio3
    temp2 = temp1.split("$")
    temp2 = temp2.sort()
    temp3 = f"${temp2[0]}"
    print("******* La mejor opcion es ********")
    if temp3 == Precio1:
        print(texto5)
    elif temp3 == Precio2:
        print(texto6)
    elif temp3 == Precio3:
        print(texto7)
    lab1.close()
    lab2.close()
    lab3.close()



def main():
    archivo = abrir_archivo("Farmaceutica_Dr_Simi.txt")
    lab1 = laboratorio_Opko("Laboratorio_Opko.txt")
    lab2 = laboratorio_Chile("Laboratorio_Chile.txt")
    lab3 = laboratorio_Pasteur("Laboratorio_Pasteur.txt")
    contenido_Farmacia(archivo,lab1,lab2,lab3)
    eleccion = -1
    
    while eleccion != 6:
        archivo = abrir_archivo("Farmaceutica_Dr_Simi.txt")
        contenido_Farmacia(archivo,lab1,lab2,lab3)
        print("1. Agregar Medicamento")
        print("2. Consultar precio Medicamento")
        print("3. Editar Medicamento")
        print("4. Eliminar medicamento")
        print("5. Mejor eleccion")
        print("6. Terminar")
        eleccion = int(input("--->"))
        if (eleccion == 1):
            eleccion = agregar_medicamento(lab1,lab2,lab3)
        elif (eleccion == 2):
            eleccion = Consultar_precio(lab1,lab2,lab3)
        elif (eleccion == 3):
            eleccion = Editar_Medicamento(lab1,lab2,lab3)
        elif (eleccion == 4):
            eleccion = Eliminar_medicamento(lab1,lab2,lab3)
        elif (eleccion == 5):
            eleccion = Mejor_eleccion(lab1,lab2,lab3)
        else:
             system("cls")
        sleep(Speed)
        print("==============================================================")
        archivo.close()

    



if __name__ == "__main__":
    main()