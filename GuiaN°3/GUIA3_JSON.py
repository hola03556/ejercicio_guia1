import json

farmacia = {}
farmacia['Laboratorio'] = []

farmacia['Laboratorio'].append({
    'nombre laboratorio': 'Laboratorio Opko',
    'Codigo': 'F-20020/18',
    'Nombre del producto': 'PARACETAMOL COMPRIMIDOS 500 mg',
    'Componente': 'PARACETAMOL',
    'Precio': '$930',
    'Codigo': 'F-19440/17',
    'Nombre del producto': 'DICLOFENACO DE SODIO COMPRIMIDOS  50 mg',
    'Componente': 'DICLOFENACO',
    'Precio': '$640',
    'Codigo': 'F-16767/18',
    'Nombre del producto': 'LORATADINA COMPRIMIDOS 10 mg',
    'Componente': 'LORATADINA:',
    'Precio': '$1.200',
    'Codigo': 'F-16587/17',
    'Nombre del producto': 'CLORFENAMINA MALEATO JARABE 2,5 mg/ 5 mL',
    'Componente': 'CLORFENAMINA',
    'Precio': '$1.040',})

farmacia['Laboratorio'].append({
    'nombre laboratorio': 'Laboratorio Chile',
    'Codigo': 'F-4001/20',
    'Nombre del producto': 'PARACETAMOL COMPRIMIDOS 500 mg',
    'Componente': 'PARACETAMOL',
    'Precio': '$3.320',
    'Codigo': 'F-3924/20',
    'Nombre del producto': 'ICLOFENACO SÓDICO ADULTOS SUPOSITORIOS 50 mg',
    'Componente': 'DICLOFENACO',
    'Precio': '$2.100',
    'Codigo': 'F-8455/21',
    'Nombre del producto': 'LORATADINA COMPRIMIDOS 10 mg',
    'Componente': 'LORATADINA',
    'Precio': '$1.500',
    'Codigo': 'F-3901/20',
    'Nombre del producto': 'CLORFENAMINA MALEATO COMPRIMIDOS 4 mg',
    'Componente': 'CLORFENAMINA',
    'Precio': '$5.860',})
    
farmacia['Laboratorio'].append({
    'nombre laboratorio': 'Laboratorio Pasteur ',
    'Codigo': 'F-22827/21',
    'Nombre del producto': 'PARACETAMOL 1 g',
    'Componente': 'PARACETAMOL',
    'Precio': '$7.290',
    'Codigo': 'F-14942/20',
    'Nombre del producto': 'DICLOFENACO SÓDICO SUPOSITORIOS 50 mg',
    'Componente': 'DICLOFENACO',
    'Precio': '$1.150',
    'Codigo': 'F-11864/17',
    'Nombre del producto': 'LORATADINA SOLUCIÓN ORAL 5 mg/5 mL',
    'Componente': 'LORATADINA',
    'Precio': '$3.940',
    'Codigo': 'F-11601/21',
    'Nombre del producto': 'CLORFENAMINA MALEATO COMPRIMIDOS 4 mg',
    'Componente': 'CLORFENAMINA',
    'Precio': '$2.140',})

with open('data.json', 'w') as file_:
    json.dump(farmacia, file_, indent=4)


with open("data.json") as archivo:
    temp = json.load(archivo)

    for item in temp:
        for subitem in temp[item]:
            print(subitem.get("nombre laboratorio"))
            print(subitem.get("Codigo"))
            print(subitem.get("Nombre del producto"))
            print(subitem.get("Componente"))
            print(subitem.get("Precio"))
