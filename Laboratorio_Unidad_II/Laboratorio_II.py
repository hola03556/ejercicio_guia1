#Llamado a las librerias necesarias
import gi
from numpy import result_type
import pandas
import json

from pandas.io.formats.format import SeriesFormatter
gi.require_version("Gtk","3.0")
from gi.repository import Gtk

#Funcion de abrir los archivos necesarios
def open_file():

    try:
        with open("drug200.csv", 'r') as archivo:
            data = pandas.read_csv(archivo)
    except IOError:
        data = []
    try:
        with("drug2001.json", 'r') as archivo_json:
            datos = json.load(archivo_json)
    except AttributeError:
            datos = []
    return data,datos

#Funcion que guarda los elementos agregados en el archivo json
def save_file(data_2):
    with open("drug2001.json", "w") as arcchivo:
        json.dump(data_2, arcchivo , indent=4) 

#Se crea la clase para la ventana principal
class Ventana_principal():

    def __init__(self):

        # Ventana Principal
        self.builder = Gtk.Builder()
        self.builder.add_from_file("Laboratorio.ui")
        ventana = self.builder.get_object("ventana_principal")
        ventana.set_title("- - - Laboratorio - - -")
        ventana.connect("destroy", Gtk.main_quit)
        ventana.set_default_size(500,400)
        
        #funcion de abrir archivo
        data_1,data_2 = open_file()

        #boton agregar
        boton_agregar = self.builder.get_object("agregar_btn")
        boton_agregar.connect("clicked", self.clicked_agregar)

        #boton resumen
        boton_resumen = self.builder.get_object("resumen_btn")
        boton_resumen.connect("clicked", self.clicked_resumen)

        #Se crea el Tree
        self.tree = self.builder.get_object("tree")
        largo_columnas = len(data_1.columns)

        #se crea el modelo
        self.modelo = Gtk.ListStore(*(largo_columnas*[str]))
        self.tree.set_model(model= self.modelo)
        cell = Gtk.CellRendererText()
        #Visualizar la ventana correspondiente
        ventana.show_all()

        #Se agrega las casillas de las categorias en el treeView
        for item in range(len(data_1.columns)):
            column = Gtk.TreeViewColumn(data_1.columns[item],cell,text = item)
            self.tree.append_column(column)
        
        #llamado de la funcion de cargar CSV en el tree para la  visualizacion del archivo
        self.load_data_from_csv()

    #Funcion cargar el archivo csv y json
    def load_data_from_csv(self):

        data_1,data_2 = open_file()
        #Se agrega cada elemento del csv y json en forma de ista en el modelo
        for item in data_1.values:
            line = [str(x) for x in item]
            self.modelo.append(line)
        for item2 in data_2:
            line2 = [y for y in item2.values()]
            self.modelo.append(line2)

    #Funcion que borra todo el contenido del modelo 
    def delete_all_data(self):
        for index in range(len(self.modelo)):
            iter_ = self.modelo.get_iter(0)
            self.modelo.remove(iter_)

    #funcion de evento de presionar el boton agregar
    def clicked_agregar(self,btn=None):
        dialogo = Ventana_secundaria()
        response = dialogo.ventana_dialogo.run()

        if response == Gtk.ResponseType.OK:
            self.delete_all_data()
            self.load_data_from_csv()
            pass
        elif response == Gtk.ResponseType.CANCEL:
            pass
        dialogo.ventana_dialogo.destroy()

    #funcion de evento de presionar el boto de resumen 
    def clicked_resumen(self,btn=None):
        dialogo_resumen = Dialogo_Resumen()
        response = dialogo_resumen.ventana_3.run()
        if response == Gtk.ResponseType.CLOSE:
            dialogo_resumen.ventana_3.destroy()

#Se crea la clase para la ventana secundaria    
class Ventana_secundaria():

    def __init__(self):
        #Se llaman los archivos necesrios para abrir y funcione la clase
        builder = Gtk.Builder()
        builder.add_from_file("Laboratorio.ui")
        #ventana secundaria
        self.ventana_dialogo = builder.get_object("ventana_secundaria")
        self.ventana_dialogo.set_title("- - - Nueva Cita - - -")
        self.ventana_dialogo.set_default_size(500,400)
        #entrada edad
        self.entrada_edad = builder.get_object("entrada_edad")
        self.entrada_edad.connect("activate",self.enter_edad)
        #entrada proporcion sodi a potasio
        self.entrada_Na_K = builder.get_object("entrada_Na/K")
        self.entrada_edad.connect("activate",self.enter_Na_K)
        boton_aceptar = builder.get_object("aceptar_btn")
        boton_aceptar.connect("clicked",self.boton_ok_clicked)
        #llamado de la funcion de abrir los archivos
        data_1,data_2 = open_file()
        #visualizacion de la ventana secundaria
        self.ventana_dialogo.show_all()
    
        #Creacion de listas necesarios par el contenido de los comboboxtext
        lista_sex = ["--Seleccione--"]
        lista_presi_art = ["--Seleccione--"]
        lista_nivel_coles = ["--Seleccione--"]
        lista_TD = ["--Seleccione--"]
        #Listas para comprobar contenidos de existentes o repetidos en cada columna
        temp1 = []
        temp2 = []
        temp3 = []
        temp4 = []

        
        #Recorrer el data por la columna de Sex para obtener 
        #cada dato unico , sin repetir
        for row1 in range(len(data_1)-1):
            if data_1.at[row1+1,"Sex"] not in temp1:
                valor = data_1.at[row1+1,"Sex"]
                valor_1 = f'{valor}'
                lista_sex.append(valor_1)
                temp1.append(valor)
        #Recorrer el data por la columna de BP para obtener 
        #cada dato unico , sin repetir
        for row2 in range(len(data_1)-1):
            if data_1.at[row2+1,"BP"] not in temp2:
                valor = data_1.at[row2+1,"BP"]
                lista_presi_art.append(valor)
                temp2.append(valor)
        
        #Recorrer el data por la columna de Cholesterol para obtener 
        #cada dato unico , sin repetir
        for row3 in range(len(data_1)-1):
            if data_1.at[row3+1,"Cholesterol"] not in temp3:
                valor = data_1.at[row3+1,"Cholesterol"]
                lista_nivel_coles.append(valor)
                temp3.append(valor)

        #Recorrer el data por la columna de Drug para obtener 
        #cada dato unico , sin repetir
        for row4 in range(len(data_1)-1):
            if data_1.at[row4+1,"Drug"] not in temp4:
                valor = data_1.at[row4+1,"Drug"]
                lista_TD.append(valor)
                temp4.append(valor)
    
        
        #Combobox Text Sex
        self.sexio = builder.get_object("comboboxtext_se")
        #Agregar cada contenido de la lista en el comboboxtext
        for item1 in lista_sex:
            self.sexio.append_text(item1)
        self.sexio.set_active(0)
        self.sexio.connect("changed", self.on_combo_changed_2)
       #Combobox Text Presion_arterial
        self.presio_art = builder.get_object("comboboxtext_PA")
        #Agregar cada contenido de la lista en el comboboxtext
        for item2 in lista_presi_art:
            self.presio_art.append_text(item2)
        self.presio_art.set_active(0)
        self.presio_art.connect("changed", self.on_combo_changed_3)
        #Combobox Text Nivel Colesterol
        self.nivel_coles = builder.get_object("comboboxtext_NC")
        #Agregar cada contenido de la lista en el comboboxtext
        for item3 in lista_nivel_coles:
            self.nivel_coles.append_text(item3)
        self.nivel_coles.set_active(0)
        self.nivel_coles.connect("changed", self.on_combo_changed_4)
        #Combobox Text Tipo de droga
        self.tipo_drog = builder.get_object("comboboxtext_TD")
        #Agregar cada contenido de la lista en el comboboxtext
        for item4 in lista_TD:
            self.tipo_drog.append_text(item4) 
        self.tipo_drog.set_active(0)
        self.tipo_drog.connect("changed", self.on_combo_changed_5)

    def enter_edad(self,enter=None):

        age_text = self.entrada_edad.get_text()
        while True:
            try:
                Na_k_text_1 = int(float(age_text))
                break
            except ValueError:
                advertencia = ventana_advertencia()
                self.entrada_edad.set_text("")
                response = advertencia.close_ventana.run()
                if response == Gtk.ResponseType.CLOSE:
                    advertencia.close_ventana.destroy()
                    break

                
    def enter_Na_K(self,enter=None):

        Na_k_text = self.entrada_Na_K.get_text()
        while True:
            try:
                Na_k_text_1 = int(float(Na_k_text))
                break
            except ValueError:
                advertencia = ventana_advertencia()
                self.entrada_Na_K.set_text("")
                response = advertencia.close_ventana.run()
                if response == Gtk.ResponseType.CLOSE:
                    advertencia.close_ventana.destroy()
                    break
       
    #Funcion de evento de cambiar al elegir un dato en el comboboxtext
    def on_combo_changed_2(self,cmb = None):
        if self.sexio.get_active_text() == "--Seleccione--":
            return
        else:
            self.valor_1 = self.sexio.get_active_text()

    #Funcion de evento de cambiar al elegir un dato en el comboboxtext
    def on_combo_changed_3(self, cmb = None):
        if self.presio_art.get_active_text() == "--Seleccione--":
            return 
        else:
            self.valor_2 = self.presio_art.get_active_text()

    #Funcion de evento de cambiar al elegir un dato en el comboboxtext
    def on_combo_changed_4(self, cmb = None):
        if self.nivel_coles.get_active_text() == "--Seleccione--":
            return 
        else:
            self.valor_3 = self.nivel_coles.get_active_text()

    #Funcion de evento de cambiar al elegir un dato en el comboboxtext
    def on_combo_changed_5(self, cmb = None):
        if self.tipo_drog.get_active_text() == "--Seleccione--":
            return 
        else:
            self.valor_4 = self.tipo_drog.get_active_text()

    #Funcion de evento de clickear el boton aceptar
    def boton_ok_clicked(self, btn = None):
        data_1,data_2 = open_file()
        #Se recoge el texto de la entrada edad
        age_text = self.entrada_edad.get_text()
        Na_k_text = self.entrada_Na_K.get_text()

        Na_k_text = self.entrada_Na_K.get_text()
        #Crea un diccionario mediante con las columnas del csv y los valores ingresados por parte del usuario
        new_data = {data_1.columns[0]: age_text,
                    data_1.columns[1]: self.valor_1,
                    data_1.columns[2]: self.valor_2,
                    data_1.columns[3]: self.valor_3,
                    data_1.columns[4]: Na_k_text,
                    data_1.columns[5]: self.valor_4
                    }
        #Se agrega el valor en el archivo json
        data_2.append(new_data)
        save_file(data_2)

#Se crea la clase para la ventana resumen
class Dialogo_Resumen():

    def __init__(self):
        #Se llaman los archivos necesrios para abrir y funcione la clase
        builder = Gtk.Builder()
        builder.add_from_file("Laboratorio.ui")
        #ventana dialogo de resumen
        self.ventana_3 = builder.get_object("ventana_dialogo_res")
        #label informacion
        label_info = builder.get_object("label_informacion")
        boton_salir = builder.get_object("salir_btn")
        self.ventana_3.set_default_size(500,400)
        #Visualizacion de la ventana
        self.ventana_3.show_all()
        #Se llama a funcion para abrir los archivos necesarios
        data_1,data_2 = open_file()
        promedio = 0
        #Ciclo para obtener todas las edades de los pacientes 
        for edades in range(len(data_1)-1):
            numero = int(float(data_1.at[edades+1,"Age"]))
            promedio += numero
        
        cantidad_de_F = 0
        cantidad_de_M = 0
        #Ciclo para obtener todas las personas masculinas y femeninas de los pacientes presentes
        for sexo_persona in range(len(data_1)-1):
            if data_1.at[sexo_persona+1,"Sex"] == "M":
                cantidad_de_F += 1
            else:
                cantidad_de_M += 1

        cantidad_de_BP = 0
        #Ciclo para obtener toda la cantidad de pacientes con presion alta
        for BP in range(len(data_1)-1):
            if data_1.at[BP+1,"BP"] == "HIGH":
                cantidad_de_BP += 1

        cantidad_de_Coles = 0
        #Ciclo para obtener toda la informacion de personas con colesterol alto
        for COLES in range(len(data_1)-1):
            if data_1.at[COLES+1,"Cholesterol"] == "HIGH":
                cantidad_de_Coles += 1
        #Creacion del texto que se presentara en la ventana resumen
        texto = f'Informacion del promedio de edades de los pacientes: {promedio/len(data_1)}\n'
        texto += f'Informacion del cantidad de pacientes sexo Femenino: {cantidad_de_F}\n'
        texto += f'Informacion del cantidad de pacientes sexo Masculino: {cantidad_de_M}\n'
        texto += f'Informacion del cantidad de pacientes Nivel De Colesterol High: {cantidad_de_Coles}\n'
        texto += f'Informacion del cantidad de pacientes Nivel De Presion Arterial High: {cantidad_de_BP}\n'
        #ingreso del texto en el label de la ventana resumen
        label_info.set_text(texto)

class ventana_advertencia():
    def __init__(self):

        builder = Gtk.Builder()
        builder.add_from_file("Laboratorio.ui")
        self.close_ventana = builder.get_object("Error_dialogo")
        boton_cerrar = builder.get_object("btn_closed")
        self.close_ventana.show_all()

if __name__ == "__main__":
    Ventana_principal()
    Gtk.main()


