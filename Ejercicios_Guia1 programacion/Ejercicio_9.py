#Nombre: Bastiám Morales
#Rut: 21.276.260-1
#Matricula: 2021430009
print("*****Teorema de Pitagoras*****")
print("°Ingrese las coordenadas del punto 1")
X1 = int(input("X1: "))
Y1 = int(input("Y1: "))

print("°Ingrese las coordenadas del punto 2")
X2 = int(input("X2: "))
Y2 = int(input("Y2: "))

a = X2 - X1
b = Y2 - Y1

print("**************************************")
print("La Longitud del Cateto de las abscisas: ",a)
print("La Longitud del Cateto de las ordenadas: ",b)
c = ((a ** 2) + (b ** 2)) ** 0.5

print("La longitud de la hipotenusa: ",c)
print("**************************************")