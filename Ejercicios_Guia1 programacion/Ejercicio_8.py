#Nombre: Bastiám Morales
#Rut: 21.276.260-1
#Matricula: 2021430009
H = int(input("Ingrese el valor de la Hipotenusa: "))
R = int(input("Ingrese el valor del cateto: "))

b = ((H**2) - (R ** 2))**0.5
AreaT = (b*(R*2))/2
print("El Area del triangulo es: ",AreaT)
AreaC = 3.141592653589793*(R**2)
print("El Area del Circulo es: ",AreaC)
AreaSC = AreaC / 2
print("El Area de la semicirculo es: ",AreaSC)
AreaTo = AreaT + AreaSC
print("Area de la figura A es de: ",AreaTo)