#Nombre: Bastián Morales Aravena
#Rut: 21.276.260.1
#Fecha: 27-09-2021

#Programa que represente el juego de loteria

import random
#numero de sorteos que ya han pasado
def sorteo_pasado(): 
    a = random.randint(1,1000)
    return a
#numeros comprenden una apuesta
def numeros_apuesta():
    b = random.randint(1,10)
    return b
#maximo de los numeros que pueden ser escogidos en una apuesta
def numeros_escogidos(C =()):
    c = random.randint(C,100)
    return c
#Numero que se pueden apostar en la apuesta
def sorteo(Ses =[], a=()):
    #ciclo que agrega cada nuevo numero en la lista
    for i in range(a):
        E = random.randint(1,a)
        Ses.append(E)
    return Ses
#Funcion que revisa los numeros que se pueden apostar si esta repetido y lo reemplaza
def jugado(T =[],a=(),b=()):
    temp = []
    for i in range(b):
        F = random.randint(1,a)
        temp.append(F)
    for i in range(b):
        for j in range(b):
            if temp[i] == temp [j]:
                del temp[i]
                F = random.randint(1,a)
                temp.append(F)
    T = temp
    return T
#Ordena los numeros de la apuesta sacados por orden creciente
def orden(Sacado=[],b=()):
    for m in range(b):
        for j in range(b-1):
            if (Sacado[j] > Sacado[j+1]):
                temp = Sacado[j]
                Sacado[j] = Sacado[j+1]
                Sacado[j+1] = temp
    return Sacado
#Imprime en la terminal el cada nuevo ingreso y los anteriores
def ingresos(a=[],b=[],B=()):

    b.append(a)   
    for num in b:
        print(num)
    print("-----------------------------------------")
    return b
#Funcion que nos pregunta si queremos seguir, por la terminal
def seguir(eleccion = ()):
     a = "X"
    #Ciclo que comprueba nuestra  eleccion de seguir
     while a != "S" and a != "s":
         a = str(input("*Desea continuar(S/N): "))
         if(a == "S" or a == "s"):
             eleccion = 1
             return eleccion
             break
         elif (a == "N" or a == "n"):
             eleccion = 0
             return eleccion
             break
         elif (a != "S" or a != "N"):
              print("Intentelo de nuevo")


def main():
    loteria = 1
    numeros_sacados = []
    Sacados = []
    lista2 = []
    B = 1
    eleccion = 0
    #Ciclo que representa el juego de loteria, mientras sea verdad la condicion
    while loteria == 1:
        print("\n-----------------------------------------")
        print("ENTRADA")
        print("-----------------------------------------")
        lista = []
        Cartola = []
        numeros_sacados =[]
        #LLamada de la funcion anteriormente mencionada
        N = sorteo_pasado()
        #Agregar la variable en la lista
        lista.append(N)
        #LLamada de la funcion anteriormente mencionada
        C = numeros_apuesta()
        #Agregar la variable en la lista
        lista.append(C)
        #LLamada de la funcion anteriormente mencionada
        K = numeros_escogidos(C)
        #Agregar la variable en la lista
        lista.append(K)
        #LLamado de la funcion que corresponde los numeros que estan en juego
        Cartola = sorteo(numeros_sacados,K)
        #Llamado de la funcion que fueron escogidos y que deben estar en la cartola
        Sacado = jugado(Sacados,K,C)
        #Llamado de la funcion que ordena los numeros escogidos
        Sacado_orde = orden(Sacado,C)
        #Llamado y inicialización de los nuevos ingresos
        lista2 = ingresos(lista,lista2,B)

        if loteria == 1:
            print("SALIDA")
            print("-----------------------------------------")
            print(Sacado_orde)
            print(Cartola)
            print("-----------------------------------------")
        
        loteria = seguir(eleccion)
        print("-----------------------------------------")
        for num in Sacado_orde:
            if num in Cartola:
                print ("Gano,felicitaciones")
                break

if __name__ == "__main__":
    main()

