import gi
gi.require_version("Gtk","3.0")
from gi.repository import Gtk



class Ejercicio_Principal():

    def __init__(self):

        #Ventana principal
        self.builder = Gtk.Builder()
        self.builder.add_from_file("Ejercicio_Guia_1_II.ui")
        ventana = self.builder.get_object("principal")
        #Se le asigna un titulo a la ventana principal
        ventana.set_title("Ejemplo_Guia_1_II")
        #Se abre la ventana principal de manera maxima
        ventana.maximize()
        #Se cierra la ventana al clickear la x
        ventana.connect("destroy", Gtk.main_quit)
        #Se ejecuta la ventana
        ventana.show_all()
        
        # Boton aceptar de la ventana principal
        boton = self.builder.get_object("boton_aceptar")
        #Conectar el boton respectivamente al hacer click en él y asignarlo con una funcion
        boton.connect("clicked", self.hice_click)

       # Boton eliminar de la ventana principal
        boton_2 = self.builder.get_object("boton_eliminar")
        #Conectar el boton respectivamente al hacer click en él y asignarlo con una funcion
        boton_2.connect("clicked", self.hice_click_2)

        # Etiqueta texto en la ventana principal
        etiqueta_Texto = self.builder.get_object("Label_texto")
        #Se le cambia el nombre del label o etiqueta
        etiqueta_Texto.set_label("Texto: ")
        
        # Etiqueta numero en la ventana principal
        etiqueta_Numero = self.builder.get_object("Label_numero")
        #Se le cambia el nombre del label o etiqueta
        etiqueta_Numero.set_label("Número: ")

        # Etiqueta resultado en la ventana principal
        self.entrada_resultado = self.builder.get_object("Resultado")
        #Se le cambia el nombre del label o etiqueta
        self.entrada_resultado.set_label("Resultado: 0")


        # entrada del texto ingresado por parte del usuario en la ventana principal 
        self.entrada = self.builder.get_object("texto")
        # Conectar una entrada respectiva al hacer enter en él y asignarlo con una funcion
        self.entrada.connect("activate", self.entrada_texto)

        # entrada del numero ingresado por parte del usuario en la ventana principal 
        self.entrada_1 = self.builder.get_object("texto_1")
        #Conectar una entrada respectiva al hacer enter en él y asignarlo con una funcion
        self.entrada_1.connect("activate", self.entrada_numero)

    #Funcion de al hacer eter en la entrada relacionada
    def entrada_texto(self, enter = None):

        #Se toma el texto ingresado y se almacena en una variable
        contenido = self.entrada.get_text()
        #Se convierte ellargo del str en un numero int
        self.numero_contenido = len(contenido)
        # que se le agrega en un str y se almacenaen una variable
        texto = f'Resultado: {self.numero_contenido}'
        #Se le ingresa en la etiqueta que mostara el resultado
        self.entrada_resultado.set_text(texto)

    #Funcion de al hacer eter en la entrada relacionada
    def entrada_numero(self, enter = None):

        #Se toma el numero  str ingresado y se almacena en una variable
        nummero_str = self.entrada_1.get_text()


        #Si el usuario no ingresa algun numero, que no me genere error 
        try:
            #Variable control al ingresar una variable 
            self.control = 0
            #Se convierte un numero ingresado en ub numero tipo int
            numero_int = int(float(nummero_str))
            #Se suma el numero int y con el largo del texto ingesado
            self.total = self.numero_contenido + numero_int
            #Se convierte en una cadena str
            self.texto = f'Resultado: {self.total}'
            #Se le ingresa en la etiqueta que mostara el resultado
            self.entrada_resultado.set_text(self.texto)

        except ValueError:

            self.control = 1
            #Se le asigna un valor por defecto y no genere error
            numero_int = 0
            self.total = self.numero_contenido + numero_int
            #Se convierte en una cadena str
            self.texto = f'Resultado: {self.total}'
            #Se le ingresa en la etiqueta que mostara el resultado
            self.entrada_resultado.set_text(self.texto)

    #Funcion de al hacer click en la boton relacionada
    def hice_click(self, btn = None):

        #Se guarda una variable la clase de la ventana dialogo
        dial = Ejercicio_Dialogo()
        #se le ingresa el texto en la entrada correspondiente en la ventana dialogo
        dial.entrada_dialogo_texto.set_text(self.entrada.get_text())
        if self.control == 0:
            #se le ingresa el numero en la entrada correspondiente en la ventana dialogo
            dial.entrada_dialogo_numero.set_text(self.entrada_1.get_text())
        elif self.control == 1:
            #se le ingresa el numero 0 en la entrada correspondiente en la ventana dialogo
            dial.entrada_dialogo_numero.set_text("0")
        #se le ingresa el resultado en la entrada correspondiente en la ventana dialogo
        dial.entrada_dialogo_resultado.set_text(self.texto)

        #SEleasigna un valor a una variable segun el evento ocurrido en la ventana dialogo
        reponse = dial.dialogo.run()

        #Condicion segun el evento ocurrido
        if reponse == Gtk.ResponseType.OK:
            self.entrada.set_text("")
            self.entrada_1.set_text("0")   
        elif reponse == Gtk.ResponseType.CANCEL:
            pass
        dial.dialogo.destroy()

    #Funcion de al hacer click en la boton relacionada
    def hice_click_2(self, btn = None):
        #Reinicio de las entradas y etiqueta
        self.entrada.set_text("")
        self.entrada_1.set_text("")
        self.entrada_resultado.set_text("Resultado: 0")



class Ejercicio_Dialogo():
    
    def __init__(self):

        builder = Gtk.Builder()
        builder.add_from_file("Ejercicio_Guia_1_II.ui")
        self.dialogo = builder.get_object("l")
        self.dialogo.show_all()

        # Boton aceptar de la ventana dialogo
        boton_3 = builder.get_object("boton_aceptar_dialogo")
        boton_3.connect("clicked", self.hice_click_dialogo)

        # Boton eliminar dialogo de la ventana dialogo
        boton_4 = builder.get_object("boton_cancel_dialogo")
        boton_4.connect("clicked", self.hice_click_2_dialogo)


        # Etiqueta del texto en la ventana dialogo
        etiqueta_Texto_dialogo = builder.get_object("label_dialogo")
        #Se le cambia el nombre del label o etiqueta
        etiqueta_Texto_dialogo.set_label("Texto ingresado: ")
        
        # Etiqueta del numero en la ventana dialogo
        etiqueta_Numero_dialogo = builder.get_object("label_1_dialogo")
        #Se le cambia el nombre del label o etiqueta
        etiqueta_Numero_dialogo.set_label("Número ingresado: ")

        # entrada del texto ingresado de la ventana principal a la ventana dialogo
        self.entrada_dialogo_texto = builder.get_object("entrada_texto_dialogo")

        # entrada del numero ingresado de la ventana principal a la ventana dialogo
        self.entrada_dialogo_numero = builder.get_object("entrada_numero_dialogo")

        # Etiquta del resultado en la ventana dialogo
        self.entrada_dialogo_resultado = builder.get_object("Resultado_dialogo")
        

    def hice_click_dialogo(self, btn = None):
        pass

    def hice_click_2_dialogo(self, btn = None):
        pass



if __name__ == "__main__":
    #Llamado de la clase pricipal
    Ejercicio_Principal()
    Gtk.main()
